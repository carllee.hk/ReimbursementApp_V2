package com.ex.test;

import com.ex.database.domain.Employee;
import com.ex.database.service.EmployeeService;
import org.junit.Assert;
import org.junit.Test;

public class LoginTest {
    @Test
    public void methodReturnsProperValues(){
        EmployeeService employeeService = new EmployeeService();
        Employee employee = new Employee();
        employee = employeeService.getUserInfo("clee");
        Integer actual = employee.getIsManager();
        Integer expected = 2;
        Assert.assertEquals("expected 2 got" + actual, expected, actual);
    }



}
