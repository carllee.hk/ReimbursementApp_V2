package com.ex.database.service;

import com.ex.database.dao.impl.EmployeeDao;
import com.ex.database.dao.CompanyDao;
import com.ex.database.domain.Employee;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class EmployeeService {
    private CompanyDao<Employee, Integer, String> dao = new EmployeeDao();

//    public List<Employee> getAllEmployee(){
//        try {
//            return dao.getAll();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

    public List<Employee> getAll(){
        try {
            return dao.getAll(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public Employee getUserInfo(String s){
        try {
            return dao.findOne(s);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
        return null;

    }

    public void update(Map data){
        try {
            dao.update(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(String s){
        try {
            dao.insert(s);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(){
        try {
            dao.delete(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
