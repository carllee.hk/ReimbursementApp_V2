package com.ex.database.service;

import com.ex.database.dao.CompanyDao;
import com.ex.database.dao.impl.ReimbursementDao;
import com.ex.database.domain.Reimbursement;

import java.sql.SQLException;
import java.util.List;

public class ReimbursementService {
    private CompanyDao<Reimbursement, Integer, String> dao = new ReimbursementDao();

    public Integer insert(Reimbursement reimbursement){
        try {
            System.out.println("Inserting data");
            dao.insert(reimbursement);
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public Integer update(Reimbursement reimbursement){
        try {
            System.out.println("Updating Data");
            dao.update(reimbursement);
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public List<Reimbursement> findRequestOnStatus(String sqlString, Integer id){
        try {
            return dao.findSome(sqlString, id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
