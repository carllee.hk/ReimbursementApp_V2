package com.ex.database.domain;

import java.util.Date;

public class Reimbursement {
    private Integer reimburesementId;
    private Employee employeeId;
    private ReimbursementType typeId;
    private ReimbursementStatus statusId;
    private String description;
    private Double moneyRequest;
    private Date creationDate;
    private Employee resolvedManager;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Reimbursement() {
    }

    public Integer getReimburesementId() {
        return reimburesementId;
    }

    public void setReimburesementId(Integer reimburesementId) {
        this.reimburesementId = reimburesementId;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    public ReimbursementType getTypeId() {
        return typeId;
    }

    public void setTypeId(ReimbursementType typeId) {
        this.typeId = typeId;
    }

    public ReimbursementStatus getStatusId() {
        return statusId;
    }

    public void setStatusId(ReimbursementStatus statusId) {
        this.statusId = statusId;
    }

    public Employee getResolvedManager() {
        return resolvedManager;
    }

    public void setResolvedManager(Employee resolvedManager) {
        this.resolvedManager = resolvedManager;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMoneyRequest() {
        return moneyRequest;
    }

    public void setMoneyRequest(Double moneyRequest) {
        this.moneyRequest = moneyRequest;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "reimburesementId=" + reimburesementId +
                ", employeeId=" + employeeId +
                ", typeId=" + typeId +
                ", statusId=" + statusId +
                ", description='" + description + '\'' +
                ", moneyRequest=" + moneyRequest +
                ", creationDate=" + creationDate +
                ", resolvedManager=" + resolvedManager +
                '}';
    }
}
