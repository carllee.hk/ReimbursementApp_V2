package com.ex.database.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static String url="jdbc:oracle:thin:@erscompanydata.cfjtzhf9yzbt.us-east-2.rds.amazonaws.com:1521:ORCL";
    private static String username = "carllee";
    private static String password = "12345678";

    //legacy support
    static{
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Connection newConnection() throws SQLException{
        return DriverManager.getConnection(url, username, password);
    }
}
