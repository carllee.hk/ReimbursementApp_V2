package com.ex.database.dao.impl;

import com.ex.database.dao.CompanyDao;
import com.ex.database.domain.Employee;
import com.ex.database.utils.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmployeeDao implements CompanyDao<Employee, Integer, String> {

//    @Override
//    public List<Employee> getAll() throws SQLException {
//        Connection c = null;
//        Employee e;
//        List<Employee> employeeList = new ArrayList<>();
//
//        try {
//            c = ConnectionUtil.newConnection();
//            c.setAutoCommit(false);
//
//            String sql = "SELECT * FROM EMPLOYEE";
//            PreparedStatement ps = c.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//
//            while (rs.next()){
//                e = new Employee();
//                e.setEmployeeId(rs.getInt("EmployeeId"));
//                e.setFirstName(rs.getString("FirstName"));
//                e.setLastName(rs.getString("LastName"));
//                e.setTitle(rs.getString("title"));
//                e.setAddress(rs.getString("Address"));
//                e.setUserName(rs.getString("username"));
//                e.setPasswords(rs.getString("passwords"));
//                e.setIsManager(rs.getInt("ISMANAGER"));
//                employeeList.add(e);
//            }
//
//            c.commit();
//            return employeeList;
//
//        } catch (SQLException e1) {
//            e1.printStackTrace();
//            if(c != null){
//                c.rollback();
//            }
//        } finally {
//            if (c != null)
//                c.close();
//        }
//        return employeeList;
//    }

    @Override
    public List<Employee> getAll(Integer id) throws SQLException {
        Connection c = null;
        Employee e;
        List<Employee> employeeList = new ArrayList<>();

        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "SELECT * FROM EMPLOYEE";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                e = new Employee();
                e.setEmployeeId(rs.getInt("EMPLOYEEID"));
                e.setFirstName(rs.getString("FIRSTNAME"));
                e.setLastName(rs.getString("LASTNAME"));
                e.setTitle(rs.getString("TITLE"));
                e.setAddress(rs.getString("ADDRESS"));
                e.setUsername(rs.getString("USERNAME"));
                e.setPasswords(rs.getString("PASSWORDS"));
                e.setIsManager(rs.getInt("ISMANAGER"));
                employeeList.add(e);
            }
            c.commit();
            return employeeList;

        } catch (SQLException e1) {
            e1.printStackTrace();
            if (c != null)
                c.rollback();
        }finally {
            if (c != null)
                c.close();
        }

        return employeeList;
    }

    @Override
    public Employee findOne(String name) throws SQLException {
        Connection c = null;
        Employee e = null;

        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "SELECT * FROM EMPLOYEE WHERE USERNAME = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1,name);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                e = new Employee();
                e.setEmployeeId(rs.getInt("EMPLOYEEID"));
                e.setFirstName(rs.getString("FIRSTNAME"));
                e.setLastName(rs.getString("LASTNAME"));
                e.setTitle(rs.getString("TITLE"));
                e.setAddress(rs.getString("ADDRESS"));
                e.setUsername(rs.getString("USERNAME"));
                e.setPasswords(rs.getString("PASSWORDS"));
                e.setIsManager(rs.getInt("ISMANAGER"));
            }
            c.commit();
            return e;

        } catch (SQLException e1) {
            e1.printStackTrace();
            if (c != null)
                c.rollback();
        }finally {
            if (c != null)
                c.close();
        }

        return e;

    }

    @Override
    public List<Employee> findSome(String sql, Integer id) throws SQLException {
        return null;
    }

//    @Override
//    public List<Employee> findSome(String id, String status) throws SQLException {
//        return null;
//    }

    @Override
    public void update(Map data) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = (String) data.get("sql");
            PreparedStatement ps = c.prepareStatement(sql);
            if (!data.containsKey("Address")){
                ps.setString(1, (String) data.get("Password"));
                ps.setInt(2, (Integer) data.get("EmployeeId"));
            } else if (!data.containsKey("Password")){
                ps.setString(1, (String) data.get("Address"));
                ps.setInt(2, (Integer) data.get("EmployeeId"));
            } else {
                ps.setString(1, (String) data.get("Address"));
                ps.setString(2, (String) data.get("Password"));
                ps.setInt(3, (Integer) data.get("EmployeeId"));
            }

            ps.executeQuery();
            c.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null){
                c.rollback();
            }
        } finally {
            if (c != null){
                c.close();
            }
        }
    }

    @Override
    public void update(Employee type) throws SQLException {

    }

    @Override
    public void insert(String s) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "INSERT INTO Employee (FIRSTNAME, LASTNAME, TITLE, ADDRESS, USERNAME, PASSWORDS)  Values (?," +
                    "?,?,?,?,?)";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, "Carl");
            ps.setString(2, "Lee");
            ps.setString(3, "Manager");
            ps.setString(4,"79 Jasper Lane");
            ps.setString(5, "clee");
            ps.setString(6, "123235");
            ps.executeQuery();
            c.commit();

        } catch (SQLException e1) {
            e1.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if(c != null){
                c.close();
            }
        }

    }

    @Override
    public void insert(Map s) throws SQLException {

    }

    @Override
    public void insert(Employee type) throws SQLException {

    }

    @Override
    public void delete(Integer id) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "DELETE FROM Employee WHERE USERID = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeQuery();
            c.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null){
                c.rollback();
            }
        } finally {
            if (c != null){
                c.close();
            }
        }


    }
}

