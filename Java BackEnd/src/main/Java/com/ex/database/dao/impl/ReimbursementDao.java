package com.ex.database.dao.impl;

import com.ex.database.dao.CompanyDao;
import com.ex.database.domain.Employee;
import com.ex.database.domain.Reimbursement;
import com.ex.database.domain.ReimbursementStatus;
import com.ex.database.domain.ReimbursementType;
import com.ex.database.utils.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReimbursementDao implements CompanyDao<Reimbursement, Integer, String> {

    @Override
    public List<Reimbursement> getAll(Integer id) throws SQLException {

        return null;
    }

    @Override
    public Reimbursement findOne(String name) throws SQLException {
        return null;
    }

    @Override
    public List<Reimbursement> findSome(String sql, Integer id) throws SQLException {
        Connection c = null;
        Reimbursement r;
        ReimbursementType reimbursementType;
        ReimbursementStatus reimbursementStatus;
        Employee resolvedManger;
        Employee employee;
        List<Reimbursement> reimbursementList = new ArrayList<>();

        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String joinStatement = "SELECT REIMBURSEMENT.*, m.FIRSTNAME, m.LASTNAME, REIMBURSEMENT_TYPE.TYPE, " +
                    "REIMBURSEMENT_STATUS.STATUS, EMPLOYEE.FIRSTNAME ef, EMPLOYEE.LASTNAME el FROM REIMBURSEMENT \n" +
                    "LEFT JOIN EMPLOYEE m ON REIMBURSEMENT.RESOLVEMANAGERID = m.EMPLOYEEID\n" +
                    "JOIN EMPLOYEE ON REIMBURSEMENT.EMPLOYEEID = EMPLOYEE.EMPLOYEEID\n" +
                    "INNER JOIN REIMBURSEMENT_STATUS ON  REIMBURSEMENT.STATUSID = REIMBURSEMENT_STATUS.STATUSID\n" +
                    "INNER JOIN REIMBURSEMENT_TYPE ON REIMBURSEMENT.TYPEID = REIMBURSEMENT_TYPE.TYPEID " + sql;
            System.out.println(joinStatement);

            PreparedStatement ps = c.prepareStatement(joinStatement);
            //String sql = "SELECT * FROM REIMBURSEMENT WHERE EMPLOYEEID = ? AND STATUSID = ?";
            if (id != null)
                ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                r = new Reimbursement();
                employee = new Employee();
                reimbursementType = new ReimbursementType();
                reimbursementStatus = new ReimbursementStatus();
                resolvedManger = new Employee();

                r.setReimburesementId(rs.getInt("ReimbursementId"));
                employee.setEmployeeId(rs.getInt("EmployeeId"));
                employee.setFirstName(rs.getString("EF"));
                employee.setLastName(rs.getString("EL"));
                r.setEmployeeId(employee);
                reimbursementType.setTypeId(rs.getInt("TypeId"));
                reimbursementType.setType(rs.getString("Type"));
                r.setTypeId(reimbursementType);
                reimbursementStatus.setStatusId(rs.getInt("StatusId"));
                reimbursementStatus.setStatus(rs.getString("Status"));
                r.setStatusId(reimbursementStatus);
                r.setDescription(rs.getString("Description"));
                r.setMoneyRequest(rs.getDouble("MoneyRequest"));
                r.setCreationDate(rs.getDate("CreateDate"));
                resolvedManger.setEmployeeId(rs.getInt("ResolveManagerId"));
                resolvedManger.setFirstName(rs.getString("FirstName"));
                resolvedManger.setLastName(rs.getString("LastName"));
                r.setResolvedManager(resolvedManger);
                reimbursementList.add(r);
            }
            System.out.println(reimbursementList);
            c.commit();
            return reimbursementList;

        } catch (SQLException e1) {
            e1.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if (c != null)
                c.close();
        }
        return reimbursementList;
    }

    @Override
    public void update(Reimbursement type) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "UPDATE REIMBURSEMENT SET STATUSID = ?, RESOLVEMANAGERID = ? WHERE REIMBURSEMENTID = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, type.getStatusId().getStatusId());
            ps.setInt(2, type.getResolvedManager().getEmployeeId());
            ps.setInt(3, type.getReimburesementId());
            ps.executeQuery();
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null){
                c.rollback();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public void update(Map data) throws SQLException {
//        Connection c = null;
//
//        try {
//            c = ConnectionUtil.newConnection();
//
//            c.setAutoCommit(false);
//
//            String sql = "UPDATE REIMBURSEMENT SET STATUSID = ?, RESOLVEMANAGERID = ? WHERE REIMBURSEMENTID = ?";
//            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, (Integer) data.get("StatusId"));
//            ps.setInt(2, (Integer) data.get("ManagerId"));
//            ps.setInt(3, (Integer) data.get("reimId"));
//            ps.executeQuery();
//            c.commit();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            if (c != null){
//                c.rollback();
//            }
//        } finally {
//            if (c != null) {
//                c.close();
//            }
//        }
    }

    @Override
    public void insert(String s) throws SQLException {

    }

    @Override
    public void insert(Map data) throws SQLException {
//        Connection c = null;
//        long currentDate = System.currentTimeMillis();
//        Date date = new Date(currentDate);
//
//        try {
//            c = ConnectionUtil.newConnection();
//
//            c.setAutoCommit(false);
//
////            String sql1 = "SELECT TYPEID FROM REIMBURSEMENT_TYPE WHERE TYPE = ?";
////            PreparedStatement ps = c.prepareStatement(sql1);
////            ps.setString(1, (String) data.get("TypeId"));
////            ResultSet s = ps.executeQuery();
////            Integer type = s.getInt("TYPEID");
//
//            String sql = "INSERT INTO REIMBURSEMENT (EMPLOYEEID, TYPEID, STATUSID, DESCRIPTION, MONEYREQUEST, " +
//                    "CREATEDATE) Values (?,?,?,?,?,?)";
//            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, (Integer) data.get("EmployeeId"));
//            ps.setInt(2, (Integer) data.get("TypeId"));
//            ps.setInt(3, (Integer) data.get("StatusId"));
//            ps.setString(4,(String) data.get("Description"));
//            ps.setDouble(5, (Double) data.get("MoneyRequest"));
//            ps.setDate(6, date);
//            ps.executeQuery();
//            c.commit();
//
//        } catch (SQLException e1) {
//            e1.printStackTrace();
//            if(c != null){
//                c.rollback();
//            }
//        } finally {
//            if(c != null){
//                c.close();
//            }
//        }


    }

    @Override
    public void insert(Reimbursement type) throws SQLException {
        Connection c = null;
        long currentDate = System.currentTimeMillis();
        Date date = new Date(currentDate);
        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "INSERT INTO REIMBURSEMENT (EMPLOYEEID, TYPEID, STATUSID, DESCRIPTION, MONEYREQUEST, " +
                    "CREATEDATE) Values (?,?,?,?,?,?)";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, type.getEmployeeId().getEmployeeId());
            ps.setInt(2, type.getTypeId().getTypeId());
            ps.setInt(3, type.getStatusId().getStatusId());
            ps.setString(4, type.getDescription());
            ps.setDouble(5, type.getMoneyRequest());
            ps.setDate(6, date);
            ps.executeQuery();
            c.commit();
        } catch (SQLException e1) {
            e1.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if (c != null){
                c.close();
            }
        }
    }


    @Override
    public void delete(Integer id) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "DELETE FROM Reimbursement WHERE REIMBURSEMENTID = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeQuery();
            c.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null){
                c.rollback();
            }
        } finally {
            if (c != null){
                c.close();
            }
        }

    }
}