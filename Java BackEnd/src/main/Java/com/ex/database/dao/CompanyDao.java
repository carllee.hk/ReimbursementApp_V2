package com.ex.database.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface CompanyDao<T, O extends Serializable, O1 extends Serializable> {
    List<T> getAll(O id) throws SQLException;
    T findOne(O1 name) throws SQLException;
    List<T> findSome(O1 sql, O id) throws SQLException;
    void update(Map map) throws SQLException;
    void update(T type) throws SQLException;
    void insert(O1 s) throws SQLException;
    void insert(Map s) throws SQLException;
    void insert(T type) throws SQLException;
    void delete(O id) throws SQLException;
}

