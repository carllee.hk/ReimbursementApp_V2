package com.ex.servlet.web;

import com.ex.database.domain.Employee;
import com.ex.database.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends baseServlet{

    EmployeeService employeeService = new EmployeeService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        Employee employee = mapper.readValue(req.getInputStream(), Employee.class);
        Employee employee1 = employeeService.getUserInfo(employee.getUsername());
        if (employee1 == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.setHeader("Content-Type","application/json");
            resp.getWriter().write(mapper.writeValueAsString("User not found"));
        } else if (!employee.getPasswords().equals(employee1.getPasswords())){
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.setHeader("Content-Type","application/json");
            resp.getWriter().write(mapper.writeValueAsString("Wrong Password"));
        } else {
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setHeader("Content-Type","application/json");
            node.put("employeeId",employee1.getEmployeeId());
            node.put("username",employee1.getUsername());
            node.put("isManager", employee1.getIsManager());
            resp.getWriter().write(mapper.writeValueAsString(node));
        }

    }
}
