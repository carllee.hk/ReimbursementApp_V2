package com.ex.servlet.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class baseServlet extends HttpServlet{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = req.getRequestURI().substring(1);
        System.out.println(url);
        String[] urlParts = url.split("/");
        for(String u:urlParts){
            System.out.println(u);
        }
        req.setAttribute("urlParts", urlParts);
        super.service(req, resp);
    }
}
