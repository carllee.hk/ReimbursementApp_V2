package com.ex.servlet.web;

import com.ex.servlet.services.EmployeeService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = {"/manager/*"})

public class ManagerServlet extends baseServlet{
    String s;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] arr = (String[]) req.getAttribute("urlParts");
//        String a = req.getReader().lines().collect(Collectors.joining());
//        System.out.println(a);

        if (arr[2].equals("all")){
            s = new EmployeeService().getData();
            if (!s.isEmpty()){
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setHeader("Content-Type","application/json");
                resp.getWriter().write(s);
            }else{
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().write(s);
            };
        }
    }
}
