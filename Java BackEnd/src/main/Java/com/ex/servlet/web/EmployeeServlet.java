package com.ex.servlet.web;

import com.ex.database.service.EmployeeService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = {"/employee/*"})

public class EmployeeServlet extends baseServlet {
    Integer status;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] arr = (String[]) req.getAttribute("urlParts");
        String a = req.getReader().lines().collect(Collectors.joining());
        System.out.println(a);
        Map<String, Object> map = new HashMap<>();
        if (arr[2].equals("update")) {
            JSONObject obj = new JSONObject(a);
            map.put("EmployeeId", obj.getInt("employeeid"));

            if ((!obj.has("Address")) & (!obj.has("Password"))){
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.setHeader("Content-Type","application/json");
                resp.getWriter().write("Update Fail");
            } else if (!obj.has("Address")){
                map.put("Password", obj.getString("Password"));
                map.put("sql", "UPDATE EMPLOYEE SET PASSWORDS = ? WHERE employeeid = ?");
                new com.ex.servlet.services.EmployeeService().updadateEmployee(map);
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setHeader("Content-Type","application/json");
                resp.getWriter().write("Update Success");
            } else if (!obj.has("Password")){
                map.put("Address", obj.getString("Address"));
                map.put("sql", "UPDATE EMPLOYEE SET ADDRESS = ? WHERE employeeid = ?");
                new com.ex.servlet.services.EmployeeService().updadateEmployee(map);
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setHeader("Content-Type","application/json");
                resp.getWriter().write("Update Success");
            } else {
                map.put("Address", obj.getString("Address"));
                map.put("Password", obj.getString("Password"));
                map.put("sql", "UPDATE EMPLOYEE SET ADDRESS = ?, PASSWORDS = ? WHERE employeeid = ?");
                new com.ex.servlet.services.EmployeeService().updadateEmployee(map);
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setHeader("Content-Type","application/json");
                resp.getWriter().write("Update Success");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] arr = (String[]) req.getAttribute("urlParts");
        String a = req.getReader().lines().collect(Collectors.joining());
        System.out.println(a);

        if (arr[2].equals("getOne")){
            String username = req.getParameter("username");
            String s = new com.ex.servlet.services.EmployeeService().getOne(username);
            System.out.println(s);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setHeader("Content-Type","application/json");
            resp.getWriter().write(s);

        }

    }
}
