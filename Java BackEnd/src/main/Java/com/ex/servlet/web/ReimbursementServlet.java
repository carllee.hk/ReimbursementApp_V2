package com.ex.servlet.web;

import com.ex.database.domain.Employee;
import com.ex.database.domain.Reimbursement;
import com.ex.database.service.ReimbursementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = {"/reimbursement/*"})

public class ReimbursementServlet extends baseServlet{
    ReimbursementService reimbursementService = new ReimbursementService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] arr = (String[]) req.getAttribute("urlParts");
        String id = req.getParameter("employeeId");
        System.out.println(id);
        String sql;
        List<Reimbursement> reimbursement = null;
        ObjectMapper mapper = new ObjectMapper();
        String status = null;

        switch (arr[2]){
            case "epreimbursement":
                sql = "WHERE REIMBURSEMENT.EMPLOYEEID = ? AND REIMBURSEMENT.STATUSID = 1";
                reimbursement = reimbursementService.findRequestOnStatus(sql, Integer.parseInt(id));
                break;
            case "erreimbursement":
                sql = "WHERE REIMBURSEMENT.EMPLOYEEID = ? AND REIMBURSEMENT.STATUSID <> 1";
                reimbursement = reimbursementService.findRequestOnStatus(sql, Integer.parseInt(id));
                break;
            case "aepending":
                sql = "WHERE REIMBURSEMENT.STATUSID = 1";
                reimbursement = reimbursementService.findRequestOnStatus(sql,null);
                break;
            case "aeresolve":
                sql = "WHERE REIMBURSEMENT.STATUSID <> 1";
                reimbursement = reimbursementService.findRequestOnStatus(sql, null);
                break;
            case "seallrequest":
                sql = "WHERE REIMBURSEMENT.EMPLOYEEID = ?";
                reimbursement = reimbursementService.findRequestOnStatus(sql, Integer.parseInt(id));
                break;
            case "reimbursement":
                sql = "WHERE REIMBURSEMENT.REIMBURSEMENTID = ?";
                reimbursement = reimbursementService.findRequestOnStatus(sql, Integer.parseInt(id));
                break;
            case "ereimbursement":
                sql = "WHERE REIMBURSEMENT.EMPLOYEEID = ?";
                reimbursement = reimbursementService.findRequestOnStatus(sql, Integer.parseInt(id));
                break;
        }
        if (!reimbursement.isEmpty()){
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setHeader("Content-Type","application/json");
            resp.getWriter().write(mapper.writeValueAsString(reimbursement));
        }else{
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.setHeader("Content-Type","application/json");
            resp.getWriter().write(mapper.writeValueAsString("Error! Try again later!"));
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer status = 1;
        ObjectMapper mapper = new ObjectMapper();
        String[] arr = (String[]) req.getAttribute("urlParts");
        Reimbursement reimbursement = mapper.readValue(req.getInputStream(), Reimbursement.class);
        System.out.println(reimbursement);

        if (arr[2].equals("new")){
            status = reimbursementService.insert(reimbursement);
        }
        if (arr[2].equals("update")){
            status = reimbursementService.update(reimbursement);
        }
        if (status == 0) {
            resp.setHeader("Content-Type", "application/json");
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().write(mapper.writeValueAsString("Success"));
        }
        else {
            resp.setHeader("Content-Type", "application/json");
            resp.getWriter().write(mapper.writeValueAsString("Error! Try again later!"));
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    //    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Integer status;
//        String[] arr = (String[]) req.getAttribute("urlParts");
//        String a = req.getReader().lines().collect(Collectors.joining());
//        System.out.println(a);
//        JSONObject obj = new JSONObject(a);
//        Map<String, Object> map = new HashMap<>();
//
//        if (arr[2].equals("new")){
//            map.put("EmployeeId", obj.getInt("employeeid"));
////            map.put("TypeId", "type");
//            map.put("TypeId", 2);
//            map.put("Description", obj.getString("description"));
//            map.put("MoneyRequest", obj.getDouble("moneyrequest"));
//            map.put("StatusId", 1);
//            status = reimbursementService.newReimbursement(map);
//            if (status == 0)
//                resp.setStatus(HttpServletResponse.SC_OK);
//            else
//                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//        }
//
//        if (arr[2].equals("update")){
//            map.put("reimId", obj.getInt("reimbursementId"));
//            map.put("ManagerId", obj.getInt("employeeid"));
//            map.put("StatusId", obj.getInt("status"));
//            status = reimbursementService.update(map);
//            if (status == 0)
//                resp.setStatus(HttpServletResponse.SC_OK);
//            else
//                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//
//        }
//    }
}
