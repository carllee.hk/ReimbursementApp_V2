package com.ex.servlet.services;

import com.ex.database.domain.Employee;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class EmployeeService {
    com.ex.database.service.EmployeeService employeeService = new com.ex.database.service.EmployeeService();
    Employee employee = new Employee();
    JSONObject json = new JSONObject();
    JSONArray jsonList = new JSONArray();
    String data;

    public Integer updadateEmployee(Map data){

            System.out.println("Updating data...");
            employeeService.update(data);
            return 0;

    }

    public String getOne(String  username) {


        try{
            employee = employeeService.getUserInfo(username);
        } catch (NullPointerException e){
            e.printStackTrace();
            return "Login Fail";
        }

        json.put("employeeId", employee.getEmployeeId());
        json.put("FirstName", employee.getFirstName());
        json.put("LastName", employee.getLastName());
        json.put("Title", employee.getTitle());
        json.put("Address", employee.getAddress());
        json.put("Username", employee.getUsername());
        data = json.toString();
        return data;


    }

    public String getData() {
        List<Employee> employeeList = employeeService.getAll();
        for (Employee e: employeeList){
            JSONObject json = new JSONObject();
            json.put("employeeId", e.getEmployeeId());
            json.put("FirstName", e.getFirstName());
            json.put("LastName", e.getLastName());
            json.put("Title", e.getTitle());
            json.put("Address", e.getAddress());
            json.put("Username", e.getUsername());
            jsonList.put(json);
        }
        data = jsonList.toString();
        System.out.println(data);
        return data;
    }
}
