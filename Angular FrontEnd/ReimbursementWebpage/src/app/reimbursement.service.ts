import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Employee} from './domain/employee';
import {Reimbursement} from "./domain/reimbursement";
import {Observable} from "rxjs/Observable";
import {LoginService} from "./login.service";

@Injectable()
export class ReimbursementService {
  reimbursementURL = 'http://localhost:8090/ra/reimbursement';
  currentUser: Employee;

  constructor(private http: HttpClient, private loginService: LoginService) {}

  insert(typeId: number, description: string, money: number) {
    const url = `${this.reimbursementURL}/new`;
    return this.http.post(url, {employeeId: {employeeId: this.loginService.getEmployee().employeeId},
      typeId: {typeId: typeId}, statusId: {statusId: 1}, description: description, moneyRequest: money});
  }
  getPending(): Observable<Reimbursement[]> {
    const url = `${this.reimbursementURL}/epreimbursement`;
    const params = new HttpParams().append('employeeId', this.loginService.getEmployee().employeeId.toString());
    return this.http.get<Reimbursement[]>(url, {params: params});
  }
  getResolve(): Observable<Reimbursement[]> {
    const url = `${this.reimbursementURL}/erreimbursement`;
    const params = new HttpParams().append('employeeId', this.loginService.getEmployee().employeeId.toString());
    return this.http.get<Reimbursement[]>(url, {params: params});
  }
  getResolveA(): Observable<Reimbursement[]> {
    const url = `${this.reimbursementURL}/aeresolve`;
    return this.http.get<Reimbursement[]>(url);
  }
  getPendingA(): Observable<Reimbursement[]> {
    const url = `${this.reimbursementURL}/aepending`;
    return this.http.get<Reimbursement[]>(url);
  }
  getO(id: number): Observable<Reimbursement> {
    const url = `${this.reimbursementURL}/reimbursement`;
    const params = new HttpParams().append('employeeId', id.toString());
    return this.http.get<Reimbursement>(url, {params: params});
  }
  getOne(id: number): Observable<Reimbursement[]> {
    const url = `${this.reimbursementURL}/reimbursement`;
    const params = new HttpParams().append('employeeId', id.toString());
    return this.http.get<Reimbursement[]>(url, {params: params});
  }
  getEmyE(id: number): Observable<Reimbursement[]> {
    const url = `${this.reimbursementURL}/ereimbursement`;
    const params = new HttpParams().append('employeeId', id.toString());
    return this.http.get<Reimbursement[]>(url, {params: params});
  }
  update(rid: number, status: number) {
    const url = `${this.reimbursementURL}/update`;
    return this.http.post(url, {resolvedManager: {employeeId: this.loginService.getEmployee().employeeId},
      reimburesementId: rid, statusId: {statusId: status}});
  }

}
