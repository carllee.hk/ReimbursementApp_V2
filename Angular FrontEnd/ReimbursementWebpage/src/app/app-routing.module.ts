import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {EmployeepageComponent} from './nav/nav.component';
import {SubmitRequestComponent} from './submit-request/submit-request.component';
import {ViewInfoComponent} from './view-info/view-info.component';
import {PendingRequestComponent} from './pending-request/pending-request.component';
import {ResolvedRequestComponent} from './resolved-request/resolved-request.component';
import {AllEmployeeComponent} from './all-employee/all-employee.component';
import {AllPendingComponent} from './all-pending/all-pending.component';
import {AllResolvedComponent} from './all-resolved/all-resolved.component';
import {ReimbursementDetailComponent} from './reimbursement-detail/reimbursement-detail.component';
import {EeimbursementComponent} from './eeimbursement/eeimbursement.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: EmployeepageComponent, canActivate: [AuthGuard],
    children: [
      {path: 'pr', component: PendingRequestComponent},
      {path: 'rr', component: ResolvedRequestComponent},
      {path: 'logout', redirectTo: '/', pathMatch: 'full'},
      {path: 'sr', component: SubmitRequestComponent},
      {path: 'ai', component: ViewInfoComponent},
      {path: 'aei', component: AllEmployeeComponent},
      {path: 'apr', component: AllPendingComponent},
      {path: 'arr', component: AllResolvedComponent},
    ]
  },
  {path: 'rdetail/:id', component: ReimbursementDetailComponent},
  {path: 'edetail/:id', component: EeimbursementComponent}

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
