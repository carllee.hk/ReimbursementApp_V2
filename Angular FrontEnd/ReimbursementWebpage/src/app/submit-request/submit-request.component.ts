import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ReimbursementService} from '../reimbursement.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-submit-request',
  templateUrl: './submit-request.component.html',
  styleUrls: ['./submit-request.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SubmitRequestComponent implements OnInit {
  model: any = {};
  types = [{value: 1, description: 'Office Supplies Expenses'}, {value: 2, description: 'Meal Expenses'}, {value: 3, description: 'Transportation Expenses'}, {value: 4, description: 'Lodging Expenses'}, {value: 5, description: 'Others'}];
  constructor(private reimbusementService: ReimbursementService, private location: Location) { }

  ngOnInit() {
  }
  newRequest(type, description, money) {
    this.reimbusementService.insert(type, description, money).subscribe(

      data => {
        console.log("Submitted Success");
        this.location.back();
        // this.router.navigate(['employee']);
      },
      err => {
        console.log('Error occurred.');
      }
    );
  }

}
