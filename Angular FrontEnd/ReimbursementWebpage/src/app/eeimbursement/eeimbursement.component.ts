import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ReimbursementService} from "../reimbursement.service";
import {ActivatedRoute} from "@angular/router";
import {Reimbursement} from "../domain/reimbursement";
import {MatSort, MatTableDataSource} from "@angular/material";

@Component({
  selector: 'app-eeimbursement',
  templateUrl: './eeimbursement.component.html',
  styleUrls: ['./eeimbursement.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EeimbursementComponent implements OnInit {
  displayedColumns = ['typeId', 'description', 'moneyRequest', 'statusId', 'creationDate', 'resolvedManage'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;
  reimbursement: Reimbursement[];

  constructor(private reimbursementService: ReimbursementService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getReimbursement();
  }

  getReimbursement() {
    const  id = +this.route.snapshot.paramMap.get('id');
    this.reimbursementService.getEmyE(id)
      .subscribe(reimburse => this.dataSource.data = reimburse);
    this.dataSource.sort = this.sort;
  }

}
