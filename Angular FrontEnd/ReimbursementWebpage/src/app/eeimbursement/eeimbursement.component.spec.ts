import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EeimbursementComponent } from './eeimbursement.component';

describe('EeimbursementComponent', () => {
  let component: EeimbursementComponent;
  let fixture: ComponentFixture<EeimbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EeimbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EeimbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
