import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {LoginService} from '../login.service';
import {Employee} from '../domain/employee';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorStateMatcher} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);

  matcher = new MyErrorStateMatcher();

  model: any = {};
  // employee = new Employee();
  titles = ['Employee', 'Manager'];

  userLogin = false;
  constructor(private  loginService: LoginService, private  router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    this.loginService.logout();
  }

  login(title, username, password) {
    this.loginService.login(username, password);
  }
}
