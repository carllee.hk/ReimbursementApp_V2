import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSort, MatTableDataSource} from "@angular/material";
import {ReimbursementService} from "../reimbursement.service";

@Component({
  selector: 'app-resolved-request',
  templateUrl: './resolved-request.component.html',
  styleUrls: ['./resolved-request.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResolvedRequestComponent implements OnInit {

  displayedColumns = ['typeId', 'description', 'moneyRequest', 'statusId', 'creationDate'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;

  constructor(private reimbursementService: ReimbursementService) {
  }

  ngOnInit() {
    this.getResolved();
  }

  getResolved() {
    this.reimbursementService.getResolve().subscribe(
      reimbursements => this.dataSource.data = reimbursements
    );
    this.dataSource.sort = this.sort;
  }
}
