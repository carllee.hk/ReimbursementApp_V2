import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolvedRequestComponent } from './resolved-request.component';

describe('ResolvedRequestComponent', () => {
  let component: ResolvedRequestComponent;
  let fixture: ComponentFixture<ResolvedRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolvedRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolvedRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
