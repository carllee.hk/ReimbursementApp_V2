import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSort, MatTableDataSource} from "@angular/material";
import {ReimbursementService} from '../reimbursement.service';

@Component({
  selector: 'app-all-pending',
  templateUrl: './all-pending.component.html',
  styleUrls: ['./all-pending.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AllPendingComponent implements OnInit {
  displayedColumns = ['typeId', 'description', 'moneyRequest', 'creationDate'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;

  constructor(private reimbursementService: ReimbursementService) {
  }
  ngOnChange() {
    this.getPending();
  }
  ngOnInit() {
    this.getPending();
  }

  getPending() {
    this.reimbursementService.getPendingA().subscribe(
      reimbursements => {
        this.dataSource.data = reimbursements;
        console.log(this.dataSource.data);
      }
    );
    this.dataSource.sort = this.sort;
  }
}
