import {Component, OnInit, ViewEncapsulation} from '@angular/core';

import {ReimbursementService} from '../reimbursement.service';
import {Router} from '@angular/router';
import {MatTabsModule} from '@angular/material';
import {LoginService} from '../login.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EmployeepageComponent implements OnInit {
  navlinks = [{path: 'sr', label: 'Submit Request'}, {path: 'ai', label: 'View Info'},
    {path: 'pr', label: 'Pending Request'}, {path: 'rr', label: 'Resolved Request'},
    {path: 'logout', label: 'Logout'}];
  navlinksM = [{path: 'apr', label: 'All Pending Request'}, {path: 'arr', label: 'All Resolved Request'},
    {path: 'aei', label: 'All Employees'},
    {path: 'logout', label: 'Logout'}];
  isLoggedIn: boolean;
  isManager: boolean;
  constructor(private reimbursementService: ReimbursementService, private router: Router, private login: LoginService) {
  }
  ngOnInit() {
    this.isLoggedIn = this.login.userlogin;
    this.isManager = this.login.isManage;
    console.log(this.isLoggedIn);
    console.log(this.isManager);
  }
}
