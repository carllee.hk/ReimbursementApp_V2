import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {Reimbursement} from '../domain/reimbursement';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {ReimbursementService} from '../reimbursement.service';
import {AlertService} from "../alert.service";

@Component({
  selector: 'app-reimbursement-detail',
  templateUrl: './reimbursement-detail.component.html',
  styleUrls: ['./reimbursement-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ReimbursementDetailComponent implements OnInit {
  reimbursement: Reimbursement;

  constructor(private route: ActivatedRoute,
              private reimbursementService: ReimbursementService,
              private router: Router,
              private location: Location,
              private alert: AlertService
  ) { }

  ngOnInit() {
    this.getReimbursement();
  }



  getReimbursement() {
    const  id = +this.route.snapshot.paramMap.get('id');
    this.reimbursementService.getOne(id)
      .subscribe(reimburse => this.reimbursement = reimburse[0]);
  }

  acceptRequest() {
    const  id = +this.route.snapshot.paramMap.get('id');
    this.reimbursementService.update(id, 2).subscribe(
      resp => {this.location.back()},
      err =>  {this.alert.error(err)}
      );
  }

  rejectRequest() {
    const  id = +this.route.snapshot.paramMap.get('id');
    this.reimbursementService.update(id, 3).subscribe(
      resp => {this.location.back()},
      err =>  {this.alert.error(err)}
      );
  }
  back() {
    this.location.back();

  }


}
