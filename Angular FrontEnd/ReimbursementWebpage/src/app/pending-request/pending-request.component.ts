import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatTableDataSource, MatSort} from '@angular/material';
import {ReimbursementService} from '../reimbursement.service';
import {Reimbursement} from '../domain/reimbursement';
// import {Observable} from 'rxjs/add/observable/merge';

@Component({
  selector: 'app-pending-request',
  templateUrl: './pending-request.component.html',
  styleUrls: ['./pending-request.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class PendingRequestComponent implements OnInit {
  displayedColumns = ['typeId', 'description', 'moneyRequest', 'creationDate'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;

  constructor(private reimbursementService: ReimbursementService) {
  }

  ngOnInit() {
    this.getPending();
  }

  getPending() {
    this.reimbursementService.getPending().subscribe(
      reimbursements => this.dataSource.data = reimbursements
    );
    this.dataSource.sort = this.sort;
  }
}
