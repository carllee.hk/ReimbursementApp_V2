import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Employee} from './domain/employee';
import {Router} from '@angular/router';
import {AlertService} from "./alert.service";

@Injectable()
export class LoginService {

  loginUrl = 'http://localhost:8090/ra/login';
  userlogin = false;
  isManage = false;

  constructor(private http: HttpClient, private router: Router, private alertService: AlertService) { }
  login(username: string, password: string) {
    const url = `${this.loginUrl}`;
    return this.http.post<Employee>(url, {username: username, passwords: password}).subscribe(
      resp => {
        console.log(resp);
        localStorage.setItem('currentUser',JSON.stringify(resp));
        localStorage.setItem('userLogin', 'true');
        this.userlogin = true;
        if (resp["isManager"] === 2){
          this.isManage = true;
          localStorage.setItem('isManager','true');
        }
        this.router.navigate(['home']);
      },
      err => {
        console.log('Error occured');
        this.alertService.error(JSON.stringify(err["error"]));
      }
    );
  }
  getEmployee(): Employee {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userLogin');
    localStorage.removeItem('isManager');
    this.userlogin = false;
    this.isManage = false;
  }
}
