import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllResolvedComponent } from './all-resolved.component';

describe('AllResolvedComponent', () => {
  let component: AllResolvedComponent;
  let fixture: ComponentFixture<AllResolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllResolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllResolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
