import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSort, MatTableDataSource} from "@angular/material";
import {ReimbursementService} from "../reimbursement.service";


@Component({
  selector: 'app-all-resolved',
  templateUrl: './all-resolved.component.html',
  styleUrls: ['./all-resolved.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AllResolvedComponent implements OnInit {
  displayedColumns = ['type', 'description', 'moneyRequest', 'status', 'creationDate', 'resolvedManager'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;

  constructor(private reimbursementService: ReimbursementService) {
  }

  ngOnChange() {
    this.getResolved();
  }
  ngOnInit() {
    this.getResolved();
  }

  getResolved() {
    this.reimbursementService.getResolveA().subscribe(
      reimbursements => {
        this.dataSource.data = reimbursements;
        console.log(this.dataSource.data);
      }
    );
    this.dataSource.sort = this.sort;
  }

}
