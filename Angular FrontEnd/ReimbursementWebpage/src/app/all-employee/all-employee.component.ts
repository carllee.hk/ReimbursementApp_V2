import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-all-employee',
  templateUrl: './all-employee.component.html',
  styleUrls: ['./all-employee.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AllEmployeeComponent implements OnInit {
  displayedColumns = ['FirstName', 'LastName', 'Title', 'Address', 'Username', 'Password'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.getPending();
  }

  getPending() {
    this.employeeService.getAll().subscribe(
      employeelist => this.dataSource.data = employeelist
    );
    this.dataSource.sort = this.sort;
  }

}
