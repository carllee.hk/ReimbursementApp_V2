import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {Employee} from '../domain/employee';
import {EmployeeService} from '../employee.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-view-info',
  templateUrl: './view-info.component.html',
  styleUrls: ['./view-info.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewInfoComponent implements OnInit {
  user: Employee;
  router: Router;
  model1: any = {};
  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.employeeService.getOne().subscribe(
      resp => this.user = resp
    );
  }
  update(address, password) {
    this.employeeService.update(address, password).subscribe();
  }

}
