import {Employee} from "./employee";

export interface Reimbursement {
  reimbursementId: number;
  employeeId: Employee;
  typeId: ReimbursementType;
  statusId: ReimbursementStatus;
  description: string;
  moneyRequest: number;
  creationDate: Date;
  resolvedManager: Employee;
}

export interface ReimbursementType {
  typeId: number;
  type: string;
}

export interface ReimbursementStatus {
  statusId: number;
  status: string;
}
