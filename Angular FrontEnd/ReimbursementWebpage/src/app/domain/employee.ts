export interface Employee {
  username: string;
  passwords: string;
  employeeId: number;
  firstName: string;
  lastName: string;
  title: string;
  address: string;
  isMananger: number;
  // resolvedManage: number;
}
