import { Injectable } from '@angular/core';
import {Employee} from './domain/employee';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {LoginService} from "./login.service";

@Injectable()
export class EmployeeService {
  currentUser: Employee;
  employeeUrl = 'http://localhost:8090/ra/employee';
  managerUrl = 'http://localhost:8090/ra/manager';


  constructor(private http: HttpClient, private loginService: LoginService) {}

  update(address: string, password: string) {
    const url = `${this.employeeUrl}/update`;
    return this.http.post(url, {employeeid: this.loginService.getEmployee().employeeId,
      Address: address, Password: password});
  }

  getAll(): Observable<Employee[]> {
    const url = `${this.managerUrl}/all`;
    return this.http.get<Employee[]>(url);
  }

  getOne(): Observable<Employee> {
    const url = `${this.employeeUrl}/getOne`;
    const params = new HttpParams().append('username', this.loginService.getEmployee().username.toString());
    return this.http.get<Employee>(url, {params: params});
  }
}
