import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatInputModule, MatTabsModule, MatSelectModule, MatSortModule} from '@angular/material';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule} from './app-routing.module';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginService} from './login.service';
import { EmployeepageComponent } from './nav/nav.component';
import { SubmitRequestComponent } from './submit-request/submit-request.component';
import { ViewInfoComponent } from './view-info/view-info.component';
import { ReimbursementService} from './reimbursement.service';
import { PendingRequestComponent } from './pending-request/pending-request.component';
import { MatTableModule} from '@angular/material';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ResolvedRequestComponent } from './resolved-request/resolved-request.component';
import { AllPendingComponent } from './all-pending/all-pending.component';
import { AllResolvedComponent } from './all-resolved/all-resolved.component';
import { AllEmployeeComponent } from './all-employee/all-employee.component';
import { EmployeeService} from './employee.service';
import { ReimbursementDetailComponent } from './reimbursement-detail/reimbursement-detail.component';
import { EeimbursementComponent } from './eeimbursement/eeimbursement.component';
import { AuthGuard} from './auth.guard';
import { HomeComponent } from './home/home.component';
import { AlertComponent } from './alert/alert.component';
import {AlertService} from "./alert.service";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmployeepageComponent,
    SubmitRequestComponent,
    ViewInfoComponent,
    PendingRequestComponent,
    ResolvedRequestComponent,
    AllPendingComponent,
    AllResolvedComponent,
    AllEmployeeComponent,
    ReimbursementDetailComponent,
    EeimbursementComponent,
    HomeComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatInputModule,
    MatTabsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [LoginService, ReimbursementService, EmployeeService, AlertService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
